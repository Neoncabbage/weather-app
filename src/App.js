//this imports the package defined in package.json
import React from "react";
import Titles from "./components/Titles";
import Form from "./components/Form";
import Weather from "./components/Weather";

const API_KEY = "ace137d298f5b89837f11607299948bd";

class App extends React.Component {
  
  state = {
    temperature: undefined,
    humidity: undefined,
    generalDescription: undefined,
    windSpeed: undefined,
    sunrise: undefined,
    sunrise: undefined,
    city: undefined,
    country: undefined,
    error: undefined
  }

  //arrow functions, when defined are bound to the component in this case App, so whenever it is referenced it will be referenced to App
  //format is: method = async (e) => {}
  //e refers the event argument passed
  getWeather = async (e) => {
    //stops the page from reloading
    e.preventDefault();

    const city = e.target.elements.city.value;
    const country = e.target.elements.country.value;
    //const api_call = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=Auckland&appid=${API_KEY}&units=metric`);
    const api_call = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${API_KEY}&units=metric`);
    const data = await api_call.json();
    console.log(data);
    if( city === true && country === true ){
      //reason why set state is used instead of defining a constant is cause it updates the variables asynchronously 
      this.setState({
        temperature: data.main.temp,
        humidity: data.main.humidity,
        generalDescription: data.weather[0].description,
        windSpeed: data.wind.speed,
        sunrise: data.sys.sunrise,
        sunset: data.sys.sunset,
        city: data.name,
        country: data.sys.country,
        error: ""
      });
    } else {
      this.setState({
        error: "Please enter a value in city and country"
      })
    }
  }

  render(){

    //render returns JSX, and can only return one parent element, so everything that you want to return must be within a single div
    return (
        <div>
          <Titles />
          <Form getWeather={this.getWeather}/>
          <Weather 
            temperature={this.state.temperature}
            humidity={this.state.humidity}
            generalDescription={this.state.generalDescription}
            windSpeed={this.state.windSpeed}
            sunrise={this.state.sunrise}
            sunset={this.state.sunset}
            city={this.state.city}
            country={this.state.country}
            error={this.state.error}
          />
        </div>
    );
  }
}

export default App;

//the purpose of react, to have multiple files which contain the UI code and then you import all of those into one single file is what gets exported on to the main index.html page