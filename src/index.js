import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//if the file is a js file you can leave off the extension
import App from './App';
import registerServiceWorker from './registerServiceWorker';
//render takes two arguments 1 name of the component and 2 the div which is the location it will render to
ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
