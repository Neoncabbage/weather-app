import React from "react";

const FormStateLess = (props) => {
    return (
        <form onSubmit={props.getWeather}>
            <input name="country" placeholder="Enter country here"></input><br/>
            <input name="city" placeholder="Enter city here"></input><br/>
            <button>Find weather</button>
        </form>
    );
}

export default FormStateLess;