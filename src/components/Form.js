import React from "react";

class Form extends React.Component {
    render() {
        return (
            <form onSubmit={this.props.getWeather}>
                <input name="country" placeholder="Enter country here"></input><br/>
                <input name="city" placeholder="Enter city here"></input><br/>
                <button>Find weather</button>
            </form>
        )
    }
}

export default Form;