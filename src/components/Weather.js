import React from "react";

class Weather extends React.Component {
    render() {
        return (
            <div>
                { this.props.country && this.props.city && <p>Location: {this.props.country} , {this.props.city}</p> }
                { this.props.sunrise && <p>Sunrise: {this.props.sunrise}</p> }
                { this.props.sunset && <p>Sunset: {this.props.sunset}</p> }
                { this.props.windSpeed && <p>Wind Speed: {this.props.windSpeed}</p> }
                { this.props.humidity && <p>Humidity: {this.props.humidity}</p> }
                { this.props.temperature && <p>Temperature: {this.props.temperature}</p> }
                { this.props.generalDescription && <p>Description: {this.props.generalDescription}</p> }
                { this.props.error && <p>{this.props.error}</p> }
            </div>
        )
    }
}

export default Weather;