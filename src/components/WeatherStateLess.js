import React from "react";

//classes which don't contain any states don't need to extend the React.Component, thus they are stateless
//"this" cannot be used since it isn't a class
const WeatherStateLess = (props) => {
    return (
        <div>
            { props.country && props.city && <p>Location: {props.country} , {props.city}</p> }
            { props.sunrise && <p>Sunrise: {props.sunrise}</p> }
            { props.sunset && <p>Sunset: {props.sunset}</p> }
            { props.windSpeed && <p>Wind Speed: {props.windSpeed}</p> }
            { props.humidity && <p>Humidity: {props.humidity}</p> }
            { props.temperature && <p>Temperature: {props.temperature}</p> }
            { props.generalDescription && <p>Description: {props.generalDescription}</p> }
            { props.error && <p>{props.error}</p> }
        </div>
    );
}

export default WeatherStateLess;